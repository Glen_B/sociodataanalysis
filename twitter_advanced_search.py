import tweepy
import config
import json


if __name__ == '__main__':
    auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
    auth.set_access_token(config.access_token, config.access_secret)
    
    api = tweepy.API(auth)
    file = open("cool.json", "a")
    
    # tweet_data = api.search(q=['Superbowl', 'Superbowl50'], rpp=10, until="2016-02-06", since="2016-02-01")
    tweet_data = api.search(q=['Superbowl', 'Superbowl50', 'geocode:49.253000,-123.111432,50mi'], rpp=2)
    for tweet in tweet_data:
        json_ = json.dumps(tweet._json, separators=(',', ': ')).replace("\n", "")
        file.write(str(json_) + ",\n")

        # print tweet._json
        
    file.close()