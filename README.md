TwitterCollector
=======

TwitterCollector is a  set of simple tools to collect data from Twitter.
It uses a library called tweepy to query, collect or stream data to and from twitter.


Version
-------

0.1

License
-------

**BSD**, see `LICENSE.txt` for further details.


Setup
-------

The setup can be simple. For example if you wanted to setup for collecting data from twitter
you should only need to update the API key with your API key. The file you need to update is
 config.py. The license settings for twitter needs to be changed.
 Update the four lines to reflect the values you have for the twitter App api key you have.


Code Example
-------

This example performs a query for tweets related to the superbowl with geographic coordinates
within 50 km of the centre of the Vancouver area. It also saves the results in a file in JSON format.


```python
import tweepy
import config
import json


if __name__ == '__main__':
    auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
    auth.set_access_token(config.access_token, config.access_secret)
    
    api = tweepy.API(auth)
    file = open("cool.json", "a")
    
    # tweet_data = api.search(q=['Superbowl', 'Superbowl50'], rpp=10, until="2016-02-06", since="2016-02-01")
    tweet_data = api.search(q=['Superbowl', 'Superbowl50', 'geocode:49.253000,-123.111432,50km'], rpp=2)
    for tweet in tweet_data:
        json_ = json.dumps(tweet._json, separators=(',', ': ')).replace("\n", "")
        file.write(str(json_) + ",\n")

        # print tweet._json
        
    file.close()
```


Bundled dependencies
--------------------

If not done so already you may need to install the **tweepy** python library.

- On windows type: pip.exe  install tweepy
- On Mac type: sudo pip install tweepy



Acknowledgements
----------------

**Authors:**

- Glen Berseth (glen@fracturedplane.com)
